%global distro  CentOS Linux
%global major   8
%global minor   3
%global date    2011

Name:           centos-linux-release
Version:        %{major}.%{minor}
Release:        1.%{date}%{?dist}
Summary:        %{distro} release files
License:        GPLv2
URL:            https://centos.org
BuildArch:      noarch

Requires:       centos-repos(%{major})
Provides:       centos-release = %{version}-%{release}

# required for a lorax run (to generate install media)
Provides:       centos-release-eula
Provides:       redhat-release-eula

# upgrade path from old release packages
Obsoletes:      centos-release < 8.2-3
Obsoletes:      centos-repos < 8.2-3
Obsoletes:      centos-userland-release < 8.2-3

# required by epel-release
Provides:       redhat-release = %{version}-%{release}

# required by dnf
# https://github.com/rpm-software-management/dnf/blob/4.2.23/dnf/const.py.in#L26
Provides:       system-release = %{version}-%{release}
Provides:       system-release(releasever) = %{major}

# required by libdnf
# https://github.com/rpm-software-management/libdnf/blob/0.48.0/libdnf/module/ModulePackage.cpp#L472
Provides:       base-module(platform:el%{major})

Source200:      EULA
Source201:      LICENSE
Source202:      Contributors

Source300:      85-display-manager.preset
Source301:      90-default.preset
Source302:      99-default-disable.preset


%description
%{distro} release files.


%install
# copy license and contributors doc here for %%license and %%doc macros
cp %{SOURCE201} %{SOURCE202} .

# create /etc/system-release and /etc/redhat-release
install -d -m 0755 %{buildroot}%{_sysconfdir}
echo "%{distro} release %{version}.%{date}" > %{buildroot}%{_sysconfdir}/centos-release
echo "Derived from Red Hat Enterprise Linux %{version}" > %{buildroot}%{_sysconfdir}/centos-release-upstream
ln -s centos-release %{buildroot}%{_sysconfdir}/system-release
ln -s centos-release %{buildroot}%{_sysconfdir}/redhat-release

# Create the os-release file
install -d -m 0755 %{buildroot}%{_prefix}/lib
cat > %{buildroot}%{_prefix}/lib/os-release << EOF
NAME="%{distro}"
VERSION="%{major}"
ID="centos"
ID_LIKE="rhel fedora"
VERSION_ID="%{major}"
PLATFORM_ID="platform:el%{major}"
PRETTY_NAME="%{distro} %{major}"
ANSI_COLOR="0;31"
CPE_NAME="cpe:/o:centos:centos:%{major}"
HOME_URL="https://centos.org/"
BUG_REPORT_URL="https://bugs.centos.org/"
CENTOS_MANTISBT_PROJECT="CentOS-%{major}"
CENTOS_MANTISBT_PROJECT_VERSION="%{major}"
EOF

# Create the symlink for /etc/os-release
ln -s ../usr/lib/os-release %{buildroot}%{_sysconfdir}/os-release

# write cpe to /etc/system/release-cpe
echo "cpe:/o:centos:centos:%{major}" > %{buildroot}%{_sysconfdir}/system-release-cpe

# create /etc/issue and /etc/issue.net
echo '\S' > %{buildroot}%{_sysconfdir}/issue
echo 'Kernel \r on an \m' >> %{buildroot}%{_sysconfdir}/issue
cp %{buildroot}%{_sysconfdir}/issue{,.net}
echo >> %{buildroot}%{_sysconfdir}/issue

# set up the dist tag macros
install -d -m 0755 %{buildroot}%{_sysconfdir}/rpm
cat > %{buildroot}%{_sysconfdir}/rpm/macros.dist << EOF
# dist macros.

%%centos_ver %{major}
%%centos %{major}
%%rhel %{major}
%%dist .el%{major}
%%el%{major} 1
EOF

# use unbranded datadir
install -d -m 0755 %{buildroot}%{_datadir}/centos-release
ln -s centos-release %{buildroot}%{_datadir}/redhat-release
install -p -m 0644 %{SOURCE200} %{buildroot}%{_datadir}/centos-release/

# copy systemd presets
install -d -m 0755 %{buildroot}%{_prefix}/lib/systemd/system-preset/
install -p -m 0644 %{_sourcedir}/*.preset %{buildroot}%{_prefix}/lib/systemd/system-preset/


%files
%license LICENSE
%doc Contributors
%{_sysconfdir}/redhat-release
%{_sysconfdir}/system-release
%{_sysconfdir}/centos-release
%{_sysconfdir}/centos-release-upstream
%config(noreplace) %{_sysconfdir}/os-release
%config %{_sysconfdir}/system-release-cpe
%config(noreplace) %{_sysconfdir}/issue
%config(noreplace) %{_sysconfdir}/issue.net
%{_sysconfdir}/rpm/macros.dist
%{_datadir}/redhat-release
%{_datadir}/centos-release
%{_prefix}/lib/os-release
%{_prefix}/lib/systemd/system-preset/*
%ifarch %{arm} aarch64
%attr(0755,root,root) %{_bindir}/rootfs-expand
%endif


%changelog
* Mon Nov 09 2020 Carl George <carl@george.computer> - 8.3-1.2011
- Update version to 8.3

* Fri Nov 06 2020 Carl George <carl@george.computer> - 8.2-4.2004
- Add provides for centos-release-eula and redhat-release-eula

* Thu Sep 03 2020 Carl George <carl@george.computer> - 8.2-3.2004
- Convert to centos-linux-release

* Fri May 15 2020 Pablo Greco <pgreco@centosproject.org> - 8-2.0.1
- Relax dependency for centos-repos
- Remove update_boot, it was never used in 8
- Add rootfs_expand to aarch64
- Bump release for 8.2

* Thu Mar 12 2020 bstinson@centosproject.org - 8-1.0.9
- Add the Devel repo to centos-release
- Install os-release(5) content to /usr/lib and have /etc/os-release be a symlink (ngompa)pr#9

* Thu Jan 02 2020 Brian Stinson <bstinson@centosproject.org> - 8-1.0.8
- Add base module platform Provides so DNF can auto-discover modular platform (ngompa)pr#6
- Switched CR repo to mirrorlist to spread the load (arrfab)pr#5

* Thu Dec 19 2019 bstinson@centosproject.org - 8-1.0.7
- Typo fixes
- Disable the HA repo by default

* Wed Dec 18 2019 Pablo Greco <pgreco@centosproject.org> - 8-1.el8
- Fix requires in armhfp

* Tue Dec 17 2019 bstinson@centosproject.org - 8-1.el8
- Add the HighAvailability repository

* Wed Aug 14 2019 Neal Gompa <ngompa@centosproject.org> 8-1.el8
- Split repositories and GPG keys out into subpackages

* Sat Aug 10 2019 Fabian Arrotin <arrfab@centos.org> 8-0.el8
- modified baseurl paths, even if disabled

* Sat Aug 10 2019 Fabian Arrotin <arrfab@centos.org> 8-0.el8
- Enabled Extras by default.
- Fixed sources paths for BaseOS/AppStream

* Sat Aug 10 2019 Brian Stinson <bstinson@centosproject.org> 8-0.el7
- Update Debuginfo and fasttrack to use releasever
- Fix CentOS-media.repo to include appstream

* Wed May 08 2019 Pablo Greco <pablo@fliagreco.com.ar> 8-0.el7
- Initial setup for CentOS-8
